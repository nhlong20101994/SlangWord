
package view;

import controller.QuanLyTiengLongController;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.DefaultTableModel;
import model.QuanLyTiengLongModel;

public class QuanLyTiengLongView extends JFrame implements ICommon {
    private QuanLyTiengLongModel qltlMD;
    private String DanhSachTimKiem[] = {"Tìm theo từ lóng", "Tìm theo định nghĩa"};
    private String[] TenCot = { "Từ lóng", "Định nghĩa"};
    private String[][] DanhSachTiengLong = new String[][]{};
    private SpringLayout Layout;
    private String TuLongMoiNgay = "";
    
    // JMenuItem
    private JMenuItem jMenuItem_ChonTepTin;
    private JMenuItem jMenuItem_ImportCSV;
    private JMenuItem jMenuItem_ExportCSV;
    private JMenuItem jMenuItem_Thoat;
    
    // JComboBox
    private JComboBox jComboBox_TimKiem;
    
     // Button
    private JButton jButton_TimKiem;
    private JButton jButton_Reset;
    private JButton jButton_History;
    private JButton jButton_Them;
    private JButton jButton_CapNhat;
    private JButton jButton_DoVuiTuLong;
    private JButton jButton_DoVuiDinhNghia;
    private JButton jButton_Xoa;
    private JButton jButton_Xem;
    
    // JTextField
    private JTextField jTextField_TimKiem;
    
    // JLabel
    private JLabel jLabel_MoiNgayMotTuLong;
    
    // JTable
    private JTable jTable_DanhSachTiengLong;
    
    public QuanLyTiengLongView() {
        initComponent();
        addComponent();
        addEvent();
    }
    
    public void Run() {
        qltlMD = new QuanLyTiengLongModel();
        TuLongMoiNgay = qltlMD.TuLongMoiNgay();
        jLabel_MoiNgayMotTuLong.setText(TuLongMoiNgay);
    }

    @Override
    public void initComponent() {
        setDefaultLookAndFeelDecorated(true);
        setTitle("Tiếng Lóng");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(700, 500);
        setLocationRelativeTo(null);
        setResizable(true);
    }

    @Override
    public void addComponent() {
        Layout = new SpringLayout();
        JPanel jPanel = new JPanel(new BorderLayout());
        
        // Thêm JCombobox sắp xếp và JTextField tìm kiếm
        int height_top = 50;
        JPanel jPanel_Top = new JPanel();
        jPanel_Top.setSize(this.getWidth(), height_top);
        jPanel_Top.setPreferredSize(new Dimension(this.getWidth(), 50));
        jPanel_Top.setLayout(Layout);
        
        jComboBox_TimKiem = new JComboBox(DanhSachTimKiem);
        jComboBox_TimKiem.setPreferredSize(new Dimension(140, height_top - 20));
        
        jTextField_TimKiem = new JTextField("");
        jTextField_TimKiem.setPreferredSize(new Dimension(250, height_top - 19));
        
        jButton_TimKiem = new JButton("Tìm kiếm");
        jButton_TimKiem.setPreferredSize(new Dimension(120, height_top - 20));
        jButton_TimKiem.setIcon(new ImageIcon((Toolkit.getDefaultToolkit().createImage(QuanLyTiengLongView.class.getResource("/public/icon/timkiemhocsinh.png")))));
        
        JSeparator separator = new JSeparator(SwingConstants.HORIZONTAL);
        separator.setPreferredSize(new Dimension(this.getWidth(), 10));
        
        Layout.putConstraint(SpringLayout.VERTICAL_CENTER, jComboBox_TimKiem, 0, SpringLayout.VERTICAL_CENTER, jPanel_Top);
        Layout.putConstraint(SpringLayout.VERTICAL_CENTER, jTextField_TimKiem, 1, SpringLayout.VERTICAL_CENTER, jPanel_Top);
        Layout.putConstraint(SpringLayout.VERTICAL_CENTER, jButton_TimKiem, 0, SpringLayout.VERTICAL_CENTER, jPanel_Top);
        
        Layout.putConstraint(SpringLayout.WEST, jComboBox_TimKiem, 10, SpringLayout.WEST, jPanel_Top);
        Layout.putConstraint(SpringLayout.WEST, jTextField_TimKiem, -1, SpringLayout.EAST, jComboBox_TimKiem);
        Layout.putConstraint(SpringLayout.EAST, jTextField_TimKiem, 2, SpringLayout.WEST, jButton_TimKiem);
        Layout.putConstraint(SpringLayout.EAST, jButton_TimKiem, -10, SpringLayout.EAST, jPanel_Top);
        Layout.putConstraint(SpringLayout.SOUTH, separator, 5, SpringLayout.SOUTH, jPanel_Top);
        Layout.putConstraint(SpringLayout.WEST, separator, 10, SpringLayout.WEST, jPanel_Top);
        Layout.putConstraint(SpringLayout.EAST, separator, -10, SpringLayout.EAST, jPanel_Top);
        
        jPanel_Top.add(jComboBox_TimKiem);
        jPanel_Top.add(jTextField_TimKiem);
        jPanel_Top.add(jButton_TimKiem);
        jPanel_Top.add(separator);
        
        // Thêm JTable hiển thị danh sách học sinh
        DefaultTableModel model = new DefaultTableModel(DanhSachTiengLong, TenCot);
        jTable_DanhSachTiengLong = new JTable(model) {
            private static final long serialVersionUID = 1L;
            public boolean isCellEditable(int row, int column) { return false; };
        };
        jTable_DanhSachTiengLong.setBounds(30, 40, 200, 300);
        jTable_DanhSachTiengLong.getTableHeader().setBackground(new Color(202, 229, 232));
        jTable_DanhSachTiengLong.getTableHeader().setPreferredSize(new Dimension(50, 40));
        jTable_DanhSachTiengLong.setRowHeight(40);
        
        JScrollPane jScrollPane_Table = new JScrollPane();
        jScrollPane_Table.setViewportView(jTable_DanhSachTiengLong);
        jTable_DanhSachTiengLong.setFillsViewportHeight(true);
        
        // Thêm các nút thao tác
        JPanel jPanel_Right = new JPanel(new BorderLayout());
        jPanel_Right.setSize(200, 220);
        jPanel_Right.setPreferredSize(new Dimension(200, 220));
        GridLayout gl = new GridLayout(4, 2, 4, 4);
        JPanel jPanel_ThaoTac = new JPanel(gl);
        //jPanel_ThaoTac.setPreferredSize(new Dimension(jPanel_Right.getWidth(), jPanel_Right.getHeight()));
        Border border_ThaoTac = BorderFactory.createTitledBorder(null, "Thao tác", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION);
        jPanel_ThaoTac.setBorder(border_ThaoTac);
        jPanel_Right.add(jPanel_ThaoTac);
        
        jButton_Reset = new JButton("<html><center>Chạy lại <br>tập tin gốc");
        jButton_History = new JButton("<html><center>Lịch sử <br>tìm kiếm");
        jButton_Them = new JButton("Thêm");
        jButton_CapNhat = new JButton("Cập nhật");
        jButton_DoVuiTuLong = new JButton("<html><center>Đố vui <br>từ lóng");
        jButton_DoVuiDinhNghia = new JButton("<html><center>Đố vui <br>định nghĩa");
        jButton_Xoa = new JButton("Xóa");
        jButton_Xem = new JButton("Xem");
        
        jPanel_ThaoTac.add(jButton_Reset);
        jPanel_ThaoTac.add(jButton_History);
        jPanel_ThaoTac.add(jButton_Them);
        jPanel_ThaoTac.add(jButton_CapNhat);
        jPanel_ThaoTac.add(jButton_DoVuiTuLong);
        jPanel_ThaoTac.add(jButton_DoVuiDinhNghia);
        jPanel_ThaoTac.add(jButton_Xoa);
        jPanel_ThaoTac.add(jButton_Xem);
        
        // Thêm random từ lóng
        JPanel jPanel_Bottom = new JPanel(Layout);
        jPanel_Bottom.setSize(this.getWidth(), 80);
        jPanel_Bottom.setPreferredSize(new Dimension(this.getWidth(), 80));
        Border border_Random_Slang = BorderFactory.createTitledBorder(null, "On this day slang word", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION);
        jPanel_Bottom.setBorder(border_Random_Slang);
        
        jLabel_MoiNgayMotTuLong = new JLabel(TuLongMoiNgay, SwingConstants.CENTER);
        Layout.putConstraint(SpringLayout.NORTH, jLabel_MoiNgayMotTuLong, 5, SpringLayout.NORTH, jPanel_Bottom);
        Layout.putConstraint(SpringLayout.WEST, jLabel_MoiNgayMotTuLong, 5, SpringLayout.WEST, jPanel_Bottom);
        Layout.putConstraint(SpringLayout.EAST, jLabel_MoiNgayMotTuLong, -5, SpringLayout.EAST, jPanel_Bottom);
        Layout.putConstraint(SpringLayout.SOUTH, jLabel_MoiNgayMotTuLong, -5, SpringLayout.SOUTH, jPanel_Bottom);
        jPanel_Bottom.add(jLabel_MoiNgayMotTuLong);
        
        // Thêm các thành phần vào JPanel chính
        jPanel.add(jPanel_Top, BorderLayout.NORTH);
        jPanel.add(jScrollPane_Table, BorderLayout.CENTER);
        jPanel.add(jPanel_Right, BorderLayout.EAST);
        jPanel.add(jPanel_Bottom, BorderLayout.SOUTH);
        
        // Thêm Jpanel vào JFrame
        add(jPanel);
    }

    @Override
    public void addEvent() {
        QuanLyTiengLongController qltlCTL = new QuanLyTiengLongController(this);
        jTextField_TimKiem.addKeyListener(qltlCTL);
        jButton_TimKiem.addActionListener(qltlCTL);
        jButton_Reset.addActionListener(qltlCTL);
        jButton_History.addActionListener(qltlCTL);
        jButton_Them.addActionListener(qltlCTL);
        jButton_CapNhat.addActionListener(qltlCTL);
        jButton_DoVuiTuLong.addActionListener(qltlCTL);
        jButton_DoVuiDinhNghia.addActionListener(qltlCTL);
        jButton_Xoa.addActionListener(qltlCTL);
        jButton_Xem.addActionListener(qltlCTL);
    }
    
    public void ThemTiengLong() {
        ThemTiengLongView ttl = new ThemTiengLongView(this, qltlMD);
        this.setEnabled(false);
        ttl.setVisible(true);
    }
    
    public void XemLichSuTimKiem() {
        LichSuTimKiemView lstk = new LichSuTimKiemView(this, qltlMD.LayLichSuTimKiem());
        this.setEnabled(false);
        lstk.setVisible(true);
    }
    
    public void TimKiemTheo_TuLong_DinhNghia() {
        String TuTimKiem = jTextField_TimKiem.getText();
        String LoaiTimKiem = jComboBox_TimKiem.getSelectedItem().toString();
        
        if(!TuTimKiem.equals("")) {
            if(LoaiTimKiem.equals("Tìm theo từ lóng")){
                CapNhatBangHocSinh(qltlMD.TimKiemTiengLong(TuTimKiem, 1));
            }
            if(LoaiTimKiem.equals("Tìm theo định nghĩa")){
                CapNhatBangHocSinh(qltlMD.TimKiemTiengLong(TuTimKiem, 2));
            }
        } else {
            if(!jButton_Xem.getText().equals("Xem")) {
                jButton_Xem.setText("Xem");
            }
            CapNhatBangHocSinh(new String[][]{});
        }
    }
    
    public void CapNhatTiengLong() {
        int index = jTable_DanhSachTiengLong.getSelectedRow();
        if(index > -1) {
            String TuLong_Chon = jTable_DanhSachTiengLong.getValueAt(index, 0).toString();
            String DinhNghia_Chon = jTable_DanhSachTiengLong.getValueAt(index, 1).toString();
            CapNhatTiengLongView cntl = new CapNhatTiengLongView(this, qltlMD, TuLong_Chon, DinhNghia_Chon);
            this.setEnabled(false);
            cntl.setVisible(true);
        }
    }
    
    public void DoVui_TuLong() {
        DoVuiView dv = new DoVuiView(this, qltlMD.LayDanhSachTiengLong(), 1);
        this.setEnabled(false);
        dv.setVisible(true);
    }
    
    public void DoVui_DinhNghia() {
        DoVuiView dv = new DoVuiView(this, qltlMD.LayDanhSachTiengLong(), 2);
        this.setEnabled(false);
        dv.setVisible(true);
    }
    
    public void XoaTiengLong() {
        int index = jTable_DanhSachTiengLong.getSelectedRow();
        if(index > -1) {
            String TuLong_Chon = jTable_DanhSachTiengLong.getValueAt(index, 0).toString();
            int result = JOptionPane.showConfirmDialog(this,
            "Bạn có chắc muốn xóa dư liệu này.",
            "Xác nhận",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE);
            if(result == JOptionPane.YES_OPTION){
                qltlMD.XoaTiengLong(TuLong_Chon);
                CapNhatBangHocSinh(qltlMD.XemDanhSachTiengLong());
                jTable_DanhSachTiengLong.getSelectionModel().clearSelection();
            }else {
                
            }
        }
    }
    
    public String getJTextField_TimKiem() {
        return jTextField_TimKiem.getText();
    }
    
    public void CapNhat_Xem_An(String button) {
        if(button.equals("Xem")) {
            jButton_Xem.setText("Ẩn");
            CapNhatBangHocSinh(qltlMD.XemDanhSachTiengLong());
        }
        if(button.equals("Ẩn")) {
            jButton_Xem.setText("Xem");
            CapNhatBangHocSinh(new String[][]{});
        }
    }
    
    public void ChayLaiTepTinGoc() {
        int result = JOptionPane.showConfirmDialog(this,
            "Bạn có chắc muốn chạy lại tệp tin gốc",
            "Xác nhận",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE);
            if(result == JOptionPane.YES_OPTION){
                qltlMD.ChayLaiTepTinGoc();
                jButton_Xem.setText("Xem");
                CapNhatBangHocSinh(new String[][]{});
            }else {
                
            }
    }
    
    public void CapNhatBangHocSinh_MacDinh(String[][] dt){
        jTable_DanhSachTiengLong.setModel(new DefaultTableModel(dt, TenCot));
    }
    
    private void CapNhatBangHocSinh(String[][] dt){
        jTable_DanhSachTiengLong.setModel(new DefaultTableModel(dt, TenCot));
    }
}
