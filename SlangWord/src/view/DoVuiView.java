
package view;

import controller.DoVuiController;
import java.awt.*;
import java.util.LinkedHashMap;
import javax.swing.*;
import model.DoVuiModel;
import model.QuanLyDoVuiModel;
import model.TiengLongModel;

public class DoVuiView extends JFrame implements ICommon {
    private QuanLyTiengLongView QLTL_V;
    private QuanLyDoVuiModel QLDV_MD;
    private DoVuiModel DV_MD;
    private int LoaiDoVui;
    
    // SpringLayout
    private SpringLayout Layout;
    
    // JTextArea
    private JTextArea jTextArea_CauHoi;
    
    // JRadioButton
    private JRadioButton jRadioButton_CauTraLoi1;
    private JRadioButton jRadioButton_CauTraLoi2;
    private JRadioButton jRadioButton_CauTraLoi3;
    private JRadioButton jRadioButton_CauTraLoi4;
    
    // JLabel
    private JLabel jLabel_KetQua;
    
    // ButtonGroup và JButton
    private ButtonGroup group;
    private JButton jButton_Random;
    
    public DoVuiView(QuanLyTiengLongView qltlV, LinkedHashMap<String, TiengLongModel> danhsachtienglong, int loaidovui) {
        QLTL_V = qltlV;
        QLDV_MD = new QuanLyDoVuiModel(danhsachtienglong, loaidovui);
        LoaiDoVui = loaidovui;
        DV_MD = QLDV_MD.LayCauDo();
        System.out.println("loaidovui: " + (loaidovui == 1 ? "Đố vui theo từ lóng" : "Đố vui theo định nghĩa"));
        initComponent();
        addComponent();
        addEvent();
    }
    
    @Override
    public void initComponent() {
        setDefaultLookAndFeelDecorated(true);
        setTitle("Tiếng Lóng");
        setSize(500, 300);
        setLocationRelativeTo(null);
        setResizable(true);
    }

    @Override
    public void addComponent() {
        Layout = new SpringLayout();
        JPanel jPanel = new JPanel(new BorderLayout());
        
        // JPanel top
        JPanel jPanel_Top = new JPanel(Layout);
        jPanel_Top.setPreferredSize(new Dimension(getSize().width, 50));
        jPanel_Top.setBackground(Color.WHITE);
        
        String CauHoi_TuLong = "Câu hỏi: Từ lóng '" + DV_MD.getCauHoi() + "' có định nghĩa là gì?";
        String CauHoi_DinhNghia = "Câu hỏi: Định nghĩa '" + DV_MD.getCauHoi() + "' có từ lóng là gì?";
        jTextArea_CauHoi = new JTextArea((LoaiDoVui == 1 ? CauHoi_TuLong : LoaiDoVui == 2 ? CauHoi_DinhNghia : ""));
        jTextArea_CauHoi.setEditable(false);

        Layout.putConstraint(SpringLayout.NORTH, jTextArea_CauHoi, 5, SpringLayout.NORTH, jPanel_Top);
        Layout.putConstraint(SpringLayout.WEST, jTextArea_CauHoi, 40, SpringLayout.WEST, jPanel_Top);
        Layout.putConstraint(SpringLayout.EAST, jTextArea_CauHoi, -40, SpringLayout.EAST, jPanel_Top);
        Layout.putConstraint(SpringLayout.SOUTH, jTextArea_CauHoi, -5, SpringLayout.SOUTH, jPanel_Top);
        jPanel_Top.add(jTextArea_CauHoi);
        
        // JPanel center
        JPanel jPanel_Center = new JPanel(Layout);
        jPanel_Center.setBackground(new Color(202, 229, 232));
        jPanel_Center.setPreferredSize(new Dimension(getSize().width, 150));
        
        JPanel jPanel_CauTraLoi = new JPanel(new GridLayout(4,1,0,5));
        jPanel_CauTraLoi.setBackground(new Color(202, 229, 232));
        
        Layout.putConstraint(SpringLayout.NORTH, jPanel_CauTraLoi, 5, SpringLayout.NORTH, jPanel_Center);
        Layout.putConstraint(SpringLayout.WEST, jPanel_CauTraLoi, 5, SpringLayout.WEST, jPanel_Center);
        Layout.putConstraint(SpringLayout.EAST, jPanel_CauTraLoi, -5, SpringLayout.EAST, jPanel_Center);
        Layout.putConstraint(SpringLayout.SOUTH, jPanel_CauTraLoi, -5, SpringLayout.SOUTH, jPanel_Center);
        
        jRadioButton_CauTraLoi1 = new JRadioButton("Đáp án 1: " + DV_MD.getDanhSachCauTraLoi().get(0));
        jRadioButton_CauTraLoi1.setPreferredSize(new Dimension(getSize().width - 100, 30));
        jRadioButton_CauTraLoi2 = new JRadioButton("Đáp án 2: " + DV_MD.getDanhSachCauTraLoi().get(1));
        jRadioButton_CauTraLoi2.setPreferredSize(new Dimension(getSize().width - 100, 30));
        jRadioButton_CauTraLoi3 = new JRadioButton("Đáp án 3: " + DV_MD.getDanhSachCauTraLoi().get(2));
        jRadioButton_CauTraLoi3.setPreferredSize(new Dimension(getSize().width - 100, 30));
        jRadioButton_CauTraLoi4 = new JRadioButton("Đáp án 4: " + DV_MD.getDanhSachCauTraLoi().get(3));
        jRadioButton_CauTraLoi4.setPreferredSize(new Dimension(getSize().width - 100, 30));
        group = new ButtonGroup();
        group.add(jRadioButton_CauTraLoi1);
        group.add(jRadioButton_CauTraLoi2);
        group.add(jRadioButton_CauTraLoi3);
        group.add(jRadioButton_CauTraLoi4);
        jPanel_CauTraLoi.add(jRadioButton_CauTraLoi1);
        jPanel_CauTraLoi.add(jRadioButton_CauTraLoi2);
        jPanel_CauTraLoi.add(jRadioButton_CauTraLoi3);
        jPanel_CauTraLoi.add(jRadioButton_CauTraLoi4);
        
        jPanel_Center.add(jPanel_CauTraLoi);
        
        // JPanel bottom
        JPanel jPanel_Bottom = new JPanel(Layout);
        jPanel_Bottom.setPreferredSize(new Dimension(getSize().width, 50));
        jPanel_Bottom.setBackground(Color.WHITE);
        
        jLabel_KetQua = new JLabel("", SwingConstants.CENTER);
        jLabel_KetQua.setSize(new Dimension(getSize().width - 100, 40));
        jLabel_KetQua.setPreferredSize(new Dimension(getSize().width - 100, 40));
        jLabel_KetQua.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        jButton_Random = new JButton("Random");
        jButton_Random.setPreferredSize(new Dimension(90, 40));
        
        JSeparator sr = new JSeparator(SwingConstants.VERTICAL);
        sr.setPreferredSize(new Dimension(2, 45));
        
        Layout.putConstraint(SpringLayout.NORTH, jLabel_KetQua, 5, SpringLayout.NORTH, jPanel_Bottom);
        Layout.putConstraint(SpringLayout.WEST, jLabel_KetQua, 5, SpringLayout.WEST, jPanel_Bottom);
        Layout.putConstraint(SpringLayout.EAST, jLabel_KetQua, -5, SpringLayout.WEST, sr);
        Layout.putConstraint(SpringLayout.SOUTH, jLabel_KetQua, -5, SpringLayout.SOUTH, jPanel_Bottom);
        
        Layout.putConstraint(SpringLayout.NORTH, sr, 5, SpringLayout.NORTH, jPanel_Bottom);
        Layout.putConstraint(SpringLayout.EAST, sr, -5, SpringLayout.WEST, jButton_Random);
        Layout.putConstraint(SpringLayout.SOUTH, sr, -5, SpringLayout.SOUTH, jPanel_Bottom);
        
        Layout.putConstraint(SpringLayout.NORTH, jButton_Random, 5, SpringLayout.NORTH, jPanel_Bottom);
        Layout.putConstraint(SpringLayout.EAST, jButton_Random, -5, SpringLayout.EAST, jPanel_Bottom);
        Layout.putConstraint(SpringLayout.SOUTH, jButton_Random, -5, SpringLayout.SOUTH, jPanel_Bottom);
        
        jPanel_Bottom.add(jLabel_KetQua);
        jPanel_Bottom.add(sr);
        jPanel_Bottom.add(jButton_Random);
        
        jPanel.add(jPanel_Top, BorderLayout.NORTH);
        jPanel.add(jPanel_Center, BorderLayout.CENTER);
        jPanel.add(jPanel_Bottom, BorderLayout.SOUTH);
        
        add(jPanel);
    }

    @Override
    public void addEvent() {
        DoVuiController lstk = new DoVuiController(this);
        addWindowListener(lstk);
        jButton_Random.addActionListener(lstk);
        jRadioButton_CauTraLoi1.addActionListener(lstk);
        jRadioButton_CauTraLoi2.addActionListener(lstk);
        jRadioButton_CauTraLoi3.addActionListener(lstk);
        jRadioButton_CauTraLoi4.addActionListener(lstk);
    }
    
    public void RaCauDo_TuDong() {
        DV_MD = QLDV_MD.LayCauDo();
        String CauHoi_TuLong = "Câu hỏi: Từ lóng '" + DV_MD.getCauHoi() + "' có định nghĩa là gì?";
        String CauHoi_DinhNghia = "Câu hỏi: Định nghĩa '" + DV_MD.getCauHoi() + "' có từ lóng là gì?";
        jTextArea_CauHoi.setText((LoaiDoVui == 1 ? CauHoi_TuLong : LoaiDoVui == 2 ? CauHoi_DinhNghia : ""));
        jRadioButton_CauTraLoi1.setText("Đáp án 1: " + DV_MD.getDanhSachCauTraLoi().get(0));
        jRadioButton_CauTraLoi2.setText("Đáp án 2: " + DV_MD.getDanhSachCauTraLoi().get(1));
        jRadioButton_CauTraLoi3.setText("Đáp án 3: " + DV_MD.getDanhSachCauTraLoi().get(2));
        jRadioButton_CauTraLoi4.setText("Đáp án 4: " + DV_MD.getDanhSachCauTraLoi().get(3));
        jLabel_KetQua.setText("");
        group.clearSelection();
        EnabledJRadioButton(true);
    }
    
    public void KiemTraKetQua(String ketqua) {
        EnabledJRadioButton(false);
        if((DV_MD.getKetQua().trim().toLowerCase().equals(ketqua.trim().toLowerCase()))) {
            jLabel_KetQua.setText("<html><center>Kết quả " + ketqua + " bạn chọn là: đúng.");
        } else {
            String XacNhan = "<html><center>Kết quả bạn chọn là: ";
            String KetQua = "<br>Kết quả đúng là: ";
            jLabel_KetQua.setText(XacNhan + "sai." + KetQua + DV_MD.getKetQua() + ".");
        }
    }
    
    public void DongCuaSo_DoVui() {
        QLTL_V.setEnabled(true);
        this.dispose();
    }
    
    private void EnabledJRadioButton(boolean flag){
        jRadioButton_CauTraLoi1.setEnabled(flag);
        jRadioButton_CauTraLoi2.setEnabled(flag);
        jRadioButton_CauTraLoi3.setEnabled(flag);
        jRadioButton_CauTraLoi4.setEnabled(flag);
    }
}
