
package view;
import controller.LichSuTimKiemController;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.DefaultTableModel;
import model.LichSuTimKiemModel;
import model.QuanLyLichSuTimKiemModel;
import model.QuanLyTiengLongModel;

public class LichSuTimKiemView extends JFrame implements ICommon {
    private QuanLyTiengLongView QLTL_V;
    private QuanLyLichSuTimKiemModel QLLSTK_MD;
    private boolean TrangThai_XemKetQua = false;
    
    private String[] TenCot = { "Từ khóa", "<html><center>Số kết quả <br> tìm kiếm", "Loại tìm kiếm", "Ngày tìm" };
    
    private String[][] KetQuaTimKiem;
    
    private SpringLayout Layout;
    
    // JTable
    private JTable jTable_LichSu;
    
    // JPanel
    private JPanel jPanel_Right;
    
    // JButton
    private JButton jButton_BoChon;
    
    // JList
    private JList jList_KetQuaTimKiem;
    
    public LichSuTimKiemView(QuanLyTiengLongView qltlV, ArrayList<LichSuTimKiemModel> lichsutimkiem) {
        QLTL_V = qltlV;
        QLLSTK_MD = new QuanLyLichSuTimKiemModel(lichsutimkiem);
        KetQuaTimKiem = QLLSTK_MD.XemLichSuTimKiem();
        initComponent();
        addComponent();
        addEvent();
    }
    
    @Override
    public void initComponent() {
        setDefaultLookAndFeelDecorated(true);
        setTitle("Lịch sử tìm kiếm");
        setSize(700, 400);
        setLocationRelativeTo(null);
        setResizable(false);
    }

    @Override
    public void addComponent() {
        Layout = new SpringLayout();
        JPanel jPanel = new JPanel(new BorderLayout());
        
        
        DefaultTableModel model = new DefaultTableModel(KetQuaTimKiem, TenCot);
        jTable_LichSu = new JTable(model) {
            private static final long serialVersionUID = 1L;
            public boolean isCellEditable(int row, int column) { return false; };
        };
        jTable_LichSu.setBounds(30, 40, 200, 300);
        jTable_LichSu.getTableHeader().setBackground(new Color(110, 195, 201));
        jTable_LichSu.getTableHeader().setPreferredSize(new Dimension(50, 40));
        jTable_LichSu.setBackground(new Color(202, 229, 232));
        jTable_LichSu.setRowHeight(30);
        jTable_LichSu.setFillsViewportHeight(true);
        
        JScrollPane sp = new JScrollPane(jTable_LichSu);
        sp.setViewportView(jTable_LichSu);
        
        // JPanel Right
        jPanel_Right = new JPanel(new BorderLayout());
        jPanel_Right.setSize(new Dimension(0, 0));
        jPanel_Right.setPreferredSize(new Dimension(0, 0));
        
        
        JPanel jPanel_BoChon = new JPanel(Layout);
        jPanel_BoChon.setPreferredSize(new Dimension(jPanel_Right.getWidth(), 30));
        
        jButton_BoChon = new JButton("Bỏ chọn");
        
        Layout.putConstraint(SpringLayout.NORTH, jButton_BoChon, 5, SpringLayout.NORTH, jPanel_BoChon);
        Layout.putConstraint(SpringLayout.WEST, jButton_BoChon, 2, SpringLayout.WEST, jPanel_BoChon);
        Layout.putConstraint(SpringLayout.EAST, jButton_BoChon, -2, SpringLayout.EAST, jPanel_BoChon);
        Layout.putConstraint(SpringLayout.SOUTH, jButton_BoChon, 0, SpringLayout.SOUTH, jPanel_BoChon);
        jPanel_BoChon.add(jButton_BoChon);
        
        JPanel jPanel_KetQuaTimKiem = new JPanel(Layout);
        Border border_ThaoTac = BorderFactory.createTitledBorder(null, "Kết quả tìm kiếm", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION);
        jPanel_KetQuaTimKiem.setBorder(border_ThaoTac);
        
        jList_KetQuaTimKiem = new JList();
        jList_KetQuaTimKiem.setSelectionBackground(Color.lightGray);
        
        Layout.putConstraint(SpringLayout.NORTH, jList_KetQuaTimKiem, 0, SpringLayout.NORTH, jPanel_KetQuaTimKiem);
        Layout.putConstraint(SpringLayout.WEST, jList_KetQuaTimKiem, 0, SpringLayout.WEST, jPanel_KetQuaTimKiem);
        Layout.putConstraint(SpringLayout.EAST, jList_KetQuaTimKiem, 0, SpringLayout.EAST, jPanel_KetQuaTimKiem);
        Layout.putConstraint(SpringLayout.SOUTH, jList_KetQuaTimKiem, 0, SpringLayout.SOUTH, jPanel_KetQuaTimKiem);
        jPanel_KetQuaTimKiem.add(jList_KetQuaTimKiem);
        
        jPanel_Right.add(jPanel_BoChon, BorderLayout.NORTH);
        jPanel_Right.add(jPanel_KetQuaTimKiem, BorderLayout.CENTER);
        
        // Thêm vào Jpanel
        jPanel.add(sp, BorderLayout.CENTER);
        jPanel.add(jPanel_Right, BorderLayout.EAST);
        
        // Thêm Jpanel vào JFrame
        add(jPanel);
    }

    @Override
    public void addEvent() {
        LichSuTimKiemController lstk = new LichSuTimKiemController(this);
        jTable_LichSu.getSelectionModel().addListSelectionListener(lstk);
        addWindowListener(lstk);
        jButton_BoChon.addActionListener(lstk);
    }
    
    public void BoChon_DongCuaSoDanhSach() {
        jTable_LichSu.getSelectionModel().clearSelection();
        jPanel_Right.setSize(new Dimension(0,0));
        jPanel_Right.setPreferredSize(new Dimension(0,0));
        jPanel_Right.revalidate();
        jPanel_Right.repaint();
        TrangThai_XemKetQua = false;
    }
    
    public void XemKetQuaTimKiem() {
        int index = jTable_LichSu.getSelectedRow();
        if(index > -1) {
            if(!TrangThai_XemKetQua) {
                jPanel_Right.setSize(new Dimension(200, this.getHeight()));
                jPanel_Right.setPreferredSize(new Dimension(200, this.getHeight()));
                jPanel_Right.revalidate();
                jPanel_Right.repaint();
            }
            String TuKhoa_Chon = jTable_LichSu.getValueAt(index, 0).toString();
            int SoLuongKetQua_Chon = Integer.parseInt(jTable_LichSu.getValueAt(index, 1).toString());
            String NgayTim_Chon = jTable_LichSu.getValueAt(index, 3).toString();
            String[] KetQua_ChuyenDoi = QLLSTK_MD.KetQuaTimKiem(QLLSTK_MD.TimKiem(TuKhoa_Chon, SoLuongKetQua_Chon, NgayTim_Chon));
            jList_KetQuaTimKiem.setListData(KetQua_ChuyenDoi);
        }
        TrangThai_XemKetQua = true;
    }
    
    public void DongCuaSo_ThemTiengLong() {
        QLTL_V.setEnabled(true);
        this.dispose();
    }
}
