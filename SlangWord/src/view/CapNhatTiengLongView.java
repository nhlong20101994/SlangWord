
package view;

import controller.CapNhatTiengLongController;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import model.QuanLyTiengLongModel;

public class CapNhatTiengLongView extends JFrame implements ICommon {
    private QuanLyTiengLongView QLTL_V;
    private QuanLyTiengLongModel QLTL_MD;
    private String TuLong_Chon;
    private String DinhNghia_Chon;
    
    // JTextField
    private JTextField jTextField_TuLong;
    private JTextField jTextField_DinhNghia;
    
    // JButton
    private JButton jButton_CapNhat;
    
    public CapNhatTiengLongView(QuanLyTiengLongView qltlV, QuanLyTiengLongModel qltlMD, String tulongString, String dinhnghia) {
        QLTL_V = qltlV;
        QLTL_MD = qltlMD;
        TuLong_Chon = tulongString;
        DinhNghia_Chon = dinhnghia;
        initComponent();
        addComponent();
        addEvent();
    }
    
    @Override
    public void initComponent() {
        setDefaultLookAndFeelDecorated(true);
        setTitle("Cập nhật từ lóng");
        setSize(400, 200);
        setLocationRelativeTo(null);
        setBackground(new Color(153, 209, 211));
        setResizable(false);
    }

    @Override
    public void addComponent() {
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setBackground(Color.BLUE);
        
        // jPanel_Top
        JSeparator separator1 = new JSeparator(SwingConstants.HORIZONTAL);
        separator1.setPreferredSize(new Dimension(this.getWidth(), 10));
        JPanel jPanel_Top = new JPanel();
        jPanel_Top.setPreferredSize(new Dimension(getSize().width, 35));
        jPanel_Top.setBackground(new Color(153, 209, 211));
        
        JLabel jLabel = new JLabel("Thêm nhiều định nghĩa có thể dùng | để phân loại");
        jPanel_Top.add(jLabel);
        jPanel_Top.add(separator1);
        
        // jPanel_Center
        JPanel jPanel_Center = new JPanel(new FlowLayout(FlowLayout.CENTER));
        jPanel_Center.setPreferredSize(new Dimension(getSize().width, 80));
        jPanel_Center.setBackground(new Color(153, 209, 211));
        
        JLabel jLabel_TuLong = new JLabel("Từ lóng");
        jLabel_TuLong.setPreferredSize(new Dimension(70, 30));
        JLabel jLabel_DinhNghia = new JLabel("Định nghĩa");
        jLabel_DinhNghia.setPreferredSize(new Dimension(70, 30));
        
        jTextField_TuLong = new JTextField(TuLong_Chon);
        jTextField_TuLong.setEditable(false);
        jTextField_TuLong.setPreferredSize(new Dimension(300, 30));
        jTextField_DinhNghia = new JTextField(DinhNghia_Chon);
        jTextField_DinhNghia.setPreferredSize(new Dimension(300, 30));
        
        jPanel_Center.add(jLabel_TuLong);
        jPanel_Center.add(jTextField_TuLong);
        jPanel_Center.add(jLabel_DinhNghia);
        jPanel_Center.add(jTextField_DinhNghia);
        
        // jPanel_Bottom
        JSeparator separator2 = new JSeparator(SwingConstants.HORIZONTAL);
        separator2.setPreferredSize(new Dimension(this.getWidth(), 10));
        JPanel jPanel_Bottom = new JPanel();
        jPanel_Bottom.setPreferredSize(new Dimension(getSize().width, 50));
        jPanel_Bottom.setBackground(new Color(153, 209, 211));
        
        jButton_CapNhat = new JButton("Cập nhật");
        jPanel_Bottom.add(separator2);
        jPanel_Bottom.add(jButton_CapNhat);
        
        jPanel.add(jPanel_Top, BorderLayout.NORTH);
        jPanel.add(jPanel_Center, BorderLayout.CENTER);
        jPanel.add(jPanel_Bottom, BorderLayout.SOUTH);
        add(jPanel);
    }

    @Override
    public void addEvent() {
        CapNhatTiengLongController cntlCTL = new CapNhatTiengLongController(this);
        addWindowListener(cntlCTL);
        jButton_CapNhat.addActionListener(cntlCTL);
    }
    
    public void CapNhatTiengLong() {
        String DinhNghia = jTextField_DinhNghia.getText();
        if(!DinhNghia.equals("")) {
            if(QLTL_MD.CapNhatTiengLong(TuLong_Chon, DinhNghia)) {
                QLTL_V.setEnabled(true);
                QLTL_V.CapNhatBangHocSinh_MacDinh(QLTL_MD.XemDanhSachTiengLong());
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(null,
                    "Định nghĩa đã tồn tại trong hệ thống.",
                    "Thông báo",
                    JOptionPane.INFORMATION_MESSAGE
                );
            }
        } else {
            JOptionPane.showMessageDialog(null,
                "Định nghĩa không được để trống!!!",
                "Thông báo",
                JOptionPane.ERROR_MESSAGE
            );
        }
    }
    
    public void DongCuaSo_CapNhatTiengLong() {
        QLTL_V.setEnabled(true);
        this.dispose();
    }
}
