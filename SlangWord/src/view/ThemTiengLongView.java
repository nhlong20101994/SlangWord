
package view;

import controller.ThemTiengLongController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import static javax.swing.JFrame.setDefaultLookAndFeelDecorated;
import javax.swing.*;
import model.QuanLyTiengLongModel;

public class ThemTiengLongView extends JFrame implements ICommon {
    private QuanLyTiengLongView QLTL_V;
    private QuanLyTiengLongModel QLTL_MD;
    
    private SpringLayout Layout;
    
    // JTextField
    private JTextField jTextField_TuLong;
    private JTextField jTextField_DinhNghia;
    
    // JButton
    private JButton jButton_Them;
    
    public ThemTiengLongView(QuanLyTiengLongView qltlV, QuanLyTiengLongModel qltlMD) {
        QLTL_V = qltlV;
        QLTL_MD = qltlMD;
        initComponent();
        addComponent();
        addEvent();
    }
    
    @Override
    public void initComponent() {
        setDefaultLookAndFeelDecorated(true);
        setTitle("Thêm một từ lóng mới");
        setSize(400, 150);
        setLocationRelativeTo(null);
        setBackground(new Color(153, 209, 211));
        setResizable(false);
    }

    @Override
    public void addComponent() {
        Layout = new SpringLayout();
        JPanel jPanel = new JPanel(Layout);
        
        JPanel jPanel_Top = new JPanel(Layout);
        jPanel_Top.setPreferredSize(new Dimension(70, 60));
        jPanel_Top.setBackground(new Color(153, 209, 211));
        JLabel jLabel_TuLong = new JLabel("Từ lóng");
        jLabel_TuLong.setPreferredSize(new Dimension(70, 30));
        JLabel jLabel_DinhNghia = new JLabel("Định nghĩa");
        jLabel_DinhNghia.setPreferredSize(new Dimension(70, 30));
        
        jTextField_TuLong = new JTextField();
        jTextField_TuLong.setPreferredSize(new Dimension(300, 30));
        jTextField_DinhNghia = new JTextField();
        jTextField_DinhNghia.setPreferredSize(new Dimension(300, 30));
        
        JPanel jPanel_Bottom = new JPanel(new FlowLayout(FlowLayout.CENTER));
        jPanel_Bottom.setPreferredSize(new Dimension(getWidth(), 35));
        jPanel_Bottom.setBackground(new Color(153, 209, 211));
        jButton_Them = new JButton("Thêm");
        jButton_Them.setPreferredSize(new Dimension(getWidth()/3, 28));
        jPanel_Bottom.add(jButton_Them);
        
        Layout.putConstraint(SpringLayout.NORTH, jPanel_Top, 0, SpringLayout.NORTH, jPanel);
        Layout.putConstraint(SpringLayout.WEST, jPanel_Top, 0, SpringLayout.WEST, jPanel);
        Layout.putConstraint(SpringLayout.EAST, jPanel_Top, 0, SpringLayout.EAST, jPanel);
        Layout.putConstraint(SpringLayout.SOUTH, jPanel_Top, 0, SpringLayout.NORTH, jPanel_Bottom);
        Layout.putConstraint(SpringLayout.WEST, jPanel_Bottom, 0, SpringLayout.WEST, jPanel);
        Layout.putConstraint(SpringLayout.EAST, jPanel_Bottom, 0, SpringLayout.EAST, jPanel);
        Layout.putConstraint(SpringLayout.SOUTH, jPanel_Bottom, 0, SpringLayout.SOUTH, jPanel);
        
        //Căn chỉnh JLabel và JTextField
        Layout.putConstraint(SpringLayout.NORTH, jLabel_TuLong, 10, SpringLayout.NORTH, jPanel_Top);
        Layout.putConstraint(SpringLayout.WEST, jLabel_TuLong, 5, SpringLayout.WEST, jPanel_Top);
        Layout.putConstraint(SpringLayout.WEST, jTextField_TuLong, 5, SpringLayout.EAST, jLabel_TuLong);
        Layout.putConstraint(SpringLayout.NORTH, jTextField_TuLong, 10, SpringLayout.NORTH, jPanel_Top);
        Layout.putConstraint(SpringLayout.EAST, jTextField_TuLong, -5, SpringLayout.EAST, jPanel_Top);
        
        Layout.putConstraint(SpringLayout.NORTH, jLabel_DinhNghia, 10, SpringLayout.SOUTH, jLabel_TuLong);
        Layout.putConstraint(SpringLayout.WEST, jLabel_DinhNghia, 5, SpringLayout.WEST, jPanel_Top);
        Layout.putConstraint(SpringLayout.WEST, jTextField_DinhNghia, 5, SpringLayout.EAST, jLabel_DinhNghia);
        Layout.putConstraint(SpringLayout.NORTH, jTextField_DinhNghia, 10, SpringLayout.SOUTH, jTextField_TuLong);
        Layout.putConstraint(SpringLayout.EAST, jTextField_DinhNghia, -5, SpringLayout.EAST, jPanel_Top);
        
        jPanel_Top.add(jLabel_TuLong);
        jPanel_Top.add(jTextField_TuLong);
        jPanel_Top.add(jLabel_DinhNghia);
        jPanel_Top.add(jTextField_DinhNghia);
        jPanel.add(jPanel_Top);
        jPanel.add(jPanel_Bottom);
        add(jPanel);
    }

    @Override
    public void addEvent() {
        ThemTiengLongController ttlCTL = new ThemTiengLongController(this);
        addWindowListener(ttlCTL);
        jButton_Them.addActionListener(ttlCTL);
    }
    
    public void ThemTiengLong() {
        String TuLong = jTextField_TuLong.getText();
        String DinhNghia = jTextField_DinhNghia.getText();
        if(!TuLong.equals("") && !DinhNghia.equals("")) {
            if(QLTL_MD.ThemTiengLong(TuLong, DinhNghia)) {
                QLTL_V.setEnabled(true);
                QLTL_V.CapNhatBangHocSinh_MacDinh(QLTL_MD.XemDanhSachTiengLong());
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(null,
                    "Từ lóng và định nghĩa đã tồn tại trong hệ thống.",
                    "Thông báo",
                    JOptionPane.INFORMATION_MESSAGE
                );
            }
        } else {
            JOptionPane.showMessageDialog(null,
                "Từ lóng và định nghĩa không được để trống!!!",
                "Thông báo",
                JOptionPane.ERROR_MESSAGE
            );
        }
    }
    
    public void DongCuaSo_ThemTiengLong() {
        QLTL_V.setEnabled(true);
        this.dispose();
    }
}
