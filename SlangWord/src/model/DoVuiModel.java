
package model;

import java.util.*;

public class DoVuiModel {
    private String CauHoi;
    private ArrayList<String> DanhSachCauTraLoi;
    private String KetQua;
    
    public DoVuiModel(String cauhoi, ArrayList<String> danhsachcautraloi, String ketqua){
        this.CauHoi = cauhoi;
        this.DanhSachCauTraLoi = danhsachcautraloi;
        this.KetQua = ketqua;
    }
    
    public String getCauHoi(){ return this.CauHoi; }
    public ArrayList<String> getDanhSachCauTraLoi(){ return this.DanhSachCauTraLoi; }
    public String getKetQua(){ return this.KetQua; }
    
    public void setCauHoi(String cauhoi){ this.CauHoi = cauhoi; }
    public void setDanhSachCauTraLoi(ArrayList<String> danhsachcautraloi){ this.DanhSachCauTraLoi = danhsachcautraloi; }
    public void setKetQua(String ketqua){ this.KetQua = ketqua; }
}
