
package model;

import java.io.Serializable;

public class TiengLongModel implements Serializable{
    private String TuLong;
    private String DinhNghia;
    private String[] DanhSach_DinhNghia;
    
    public TiengLongModel(String tulong, String dinhnghia, String[] danhsach_dinhnghia){
        this.TuLong = tulong;
        this.DinhNghia = dinhnghia;
        this.DanhSach_DinhNghia = danhsach_dinhnghia;
    }
    
    
    public String getTuLong(){ return this.TuLong; }
    public String getDinhNghia(){ return this.DinhNghia; }
    public String[] getDanhSachDinhNghia(){ return this.DanhSach_DinhNghia; }
    
    public void setTuLong(String tulong){ this.TuLong = tulong; }
    public void setDinhNghia(String dinhnghia){ this.DinhNghia = dinhnghia; }
    public void setDanhSachDinhNghia(String[] danhsach_dinhnghia){ this.DanhSach_DinhNghia = danhsach_dinhnghia; }
}
