
package model;

import java.util.*;

public class QuanLyLichSuTimKiemModel {
    private ArrayList<LichSuTimKiemModel> LichSuTimKiem;
    
    public QuanLyLichSuTimKiemModel(ArrayList<LichSuTimKiemModel> lichsutimkiem) {
        LichSuTimKiem = lichsutimkiem;
    }
    
    public String[] KetQuaTimKiem(LichSuTimKiemModel lstk) {
        LinkedHashMap<String, TiengLongModel> KetQua = lstk.getKetQua();
        Set<String> keySet = KetQua.keySet();
        
        String[] KetQua_ChuyenDoi = new String[KetQua.size()];
        int i = 0;
        for (String key : keySet) {
            TiengLongModel tl = KetQua.get(key);
            KetQua_ChuyenDoi[i] = tl.getTuLong() + " --- " + tl.getDinhNghia();
            i++;
        }
        return KetQua_ChuyenDoi;
    }
    
    public LichSuTimKiemModel TimKiem(String tukhoaString, int soluongketqua, String ngaytim){
        for (LichSuTimKiemModel lstk : LichSuTimKiem) {
            if(lstk.getTuKhoaTimKiem().equals(tukhoaString) && lstk.getSoLuongKetQua() == soluongketqua && lstk.getNgayTimKiem().equals(ngaytim))
                return lstk;
        }
        return null;
    }
    
    public String[][] XemLichSuTimKiem() {
        String[][] LSTK = null;
        if (LichSuTimKiem.size() <= 0) {
            return new String[][]{};
        }
        LSTK = new String[LichSuTimKiem.size()][4];
        int i = 0;
        for (LichSuTimKiemModel lstk : LichSuTimKiem) {
            LSTK[i] = new String[] {lstk.getTuKhoaTimKiem(), lstk.getSoLuongKetQua()+"", lstk.getLoaiTimKiem(), lstk.getNgayTimKiem()};
            i++;
        }
        return LSTK;
    }
}
