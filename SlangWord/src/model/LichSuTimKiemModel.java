
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class LichSuTimKiemModel implements Serializable {
    private String TuKhoaTimKiem;
    private int SoLuongKetQua;
    private LinkedHashMap<String, TiengLongModel> KetQua;
    private String LoaiTimKiem;
    private String NgayTimKiem;
    
    public LichSuTimKiemModel() {}
    
    public LichSuTimKiemModel(String tukhoatimkiem, int soluongketqua, LinkedHashMap<String, TiengLongModel> ketqua, String loaitimkiem, String ngaytimkiem){
        this.TuKhoaTimKiem = tukhoatimkiem;
        this.SoLuongKetQua = soluongketqua;
        this.KetQua = ketqua;
        this.LoaiTimKiem = loaitimkiem;
        this.NgayTimKiem = ngaytimkiem;
    }
    
    public String getTuKhoaTimKiem(){ return this.TuKhoaTimKiem; }
    public int getSoLuongKetQua(){ return this.SoLuongKetQua; }
    public LinkedHashMap<String, TiengLongModel> getKetQua(){ return this.KetQua; }
    public String getLoaiTimKiem(){ return this.LoaiTimKiem; }
    public String getNgayTimKiem(){ return this.NgayTimKiem; }
    
    public void setTuKhoaTimKiem(String tukhoatimkiem){ this.TuKhoaTimKiem = tukhoatimkiem; }
    public void setSoLuongKetQua(int soluongketqua){ this.SoLuongKetQua = soluongketqua; }
    public void setKetQua(LinkedHashMap<String, TiengLongModel> ketqua){ this.KetQua = ketqua; }
    public void setLoaiTimKiem(String loaitimkiem){ this.LoaiTimKiem = loaitimkiem; }
    public void setNgayTimKiem(String ngaytimkiem){ this.NgayTimKiem = ngaytimkiem; }
    
}
