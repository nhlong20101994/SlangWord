
package model;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QuanLyTiengLongModel {
    private final String TapTin = "/data";
    private final String TapTin_DuLieuChuaXuLy = "src/data/slang.txt";
    private final String TapTin_DaXuLy = "src/data/tulong_daxuly.txt";
    private final String TapTin_LichSuTimKiem = "src/data/lichsu_timkiem.txt";
    private final String TapTin_MoiNgayMotTuLong = "src/data/moingay_mottienglong.txt";
    private LinkedHashMap<String, TiengLongModel> DanhSachTiengLong;
    private ArrayList<LichSuTimKiemModel> LichSuTimKiem = new ArrayList<>();
    
    public QuanLyTiengLongModel() {
        TaoTapTin(TapTin, 1);
        TaoTapTin(TapTin_LichSuTimKiem, 2);
        TaoTapTin(TapTin_MoiNgayMotTuLong, 2);
        DanhSachTiengLong = KhoiTao_LamSach_DuLieu();
        LichSuTimKiem = DocDuLieu_LichSu_TimKiem(TapTin_LichSuTimKiem);
    }
    
    public String[][] XemDanhSachTiengLong() {
        return ChuyenDoiDuLieu(DanhSachTiengLong);
    }
    
    public String TuLongMoiNgay() {
        String TLMN = DocDuLieu_MoiNgayMotTuLong(TapTin_MoiNgayMotTuLong);
        String NgayThangNam_HienTai = LayNgayThangNam("dd/MM/yyyy");
        if(TLMN.equals("")) {
            String TuLong = Random_TuLong();
            String TuLong_Ngay = TuLong + "::" + NgayThangNam_HienTai;
            String[] TuLong_KetQua = TuLong.split("::");
            GhiDuLieu_MoiNgayMotTuLong(TuLong_Ngay, TapTin_MoiNgayMotTuLong);
            return "<html><center>Từ lóng:  " + TuLong_KetQua[0]  + "<br>Định nghĩa: " + TuLong_KetQua[1];
        }
        String NgayThangNam_Luu = TLMN.substring(TLMN.length() - 10);
        if(!NgayThangNam_Luu.equals(NgayThangNam_HienTai)) {
            String TuLong = Random_TuLong();
            String TuLong_Ngay = TuLong + "::" + NgayThangNam_HienTai;
            String[] TuLong_KetQua = TuLong.split("::");
            GhiDuLieu_MoiNgayMotTuLong(TuLong_Ngay, TapTin_MoiNgayMotTuLong);
            return "<html><center>Từ lóng:  " + TuLong_KetQua[0]  + "<br>Định nghĩa: " + TuLong_KetQua[1];
        }
        String[] TuLong_KetQua = TLMN.split("::");
        return "<html><center>Từ lóng:  " + TuLong_KetQua[0]  + "<br>Định nghĩa: " + TuLong_KetQua[1];
    }
    
    private String Random_TuLong() {
        ArrayList<String> DanhSachTuLong = new ArrayList<>(DanhSachTiengLong.keySet());
        Collections.shuffle(DanhSachTuLong);
        TiengLongModel TiengLong = DanhSachTiengLong.get(DanhSachTuLong.get(0));
        return TiengLong.getTuLong() + "::" + TiengLong.getDinhNghia();
    }
    
    public void XoaTiengLong(String tulong) {
        DanhSachTiengLong.remove(tulong);
        GhiDuLieu_TiengLong(DanhSachTiengLong, TapTin_DaXuLy);
    }
    
    public boolean CapNhatTiengLong(String tulong, String dinhnghia) {
        String[] DanhSach_DinhNghia = dinhnghia.split("\\|");
        TiengLongModel tl = new TiengLongModel(tulong, dinhnghia, DanhSach_DinhNghia);
        DanhSachTiengLong.replace(tulong, tl);
        GhiDuLieu_TiengLong(DanhSachTiengLong, TapTin_DaXuLy);
        return true;
    }
    
    public String[][] TimKiemTiengLong(String tukhoa, int loaitimkiem) {
        LinkedHashMap<String, TiengLongModel> KetQuaTimKiem = TimKiemTiengLongTheo_TuLong_DinhNghia(tukhoa, loaitimkiem);
        
        String LoaiTimKiem = loaitimkiem == 1 ? "Tìm theo từ lóng" : loaitimkiem == 2 ? "Tìm theo định nghĩa" : "";
        int SoLuongKetQua = KetQuaTimKiem.size();
        String NgayTimKiem = LayNgayThangNam("dd/MM/yyyy 'at' hh:mm a");
        
        LichSuTimKiemModel LichSuTimKiem_Moi = new LichSuTimKiemModel(tukhoa, SoLuongKetQua, KetQuaTimKiem, LoaiTimKiem, NgayTimKiem);
        LichSuTimKiem.add(LichSuTimKiem_Moi);
        GhiDuLieu_LichSu_TimKiem(LichSuTimKiem, TapTin_LichSuTimKiem);
        return ChuyenDoiDuLieu(KetQuaTimKiem);
    }
    
    public ArrayList<LichSuTimKiemModel> LayLichSuTimKiem() {
        return LichSuTimKiem;
    }
    
    public LinkedHashMap<String, TiengLongModel> LayDanhSachTiengLong() {
        return DanhSachTiengLong;
    }
    
    public void ChayLaiTepTinGoc() {
        File file_daxuly = new File(TapTin_DaXuLy);
        File file_lichsu = new File(TapTin_LichSuTimKiem);
        if (file_daxuly.exists()) {
            file_daxuly.delete();
        }
        if (file_lichsu.exists()) {
            file_lichsu.delete();
        }
        DanhSachTiengLong = KhoiTao_LamSach_DuLieu();
        TaoTapTin(TapTin_LichSuTimKiem, 2);
        LichSuTimKiem = DocDuLieu_LichSu_TimKiem(TapTin_LichSuTimKiem);
    }
    
    public boolean ThemTiengLong(String tulong, String dinhnghia) {
        if(DanhSachTiengLong.containsKey(tulong)) {
            TiengLongModel tl_tontai = DanhSachTiengLong.get(tulong);
            
            if(!KiemTra_DinhNghia_TonTai_DanhSachDinhNghia(tl_tontai.getDanhSachDinhNghia(), dinhnghia)) {
                String DinhNghia_Moi = tl_tontai.getDinhNghia() + " | " + dinhnghia;
                String[] DanhSach_DinhNghia_Moi = DinhNghia_Moi.split("\\|");
                TiengLongModel tl = new TiengLongModel(tulong, DinhNghia_Moi, DanhSach_DinhNghia_Moi);
                
                DanhSachTiengLong.replace(tulong, tl);
                GhiDuLieu_TiengLong(DanhSachTiengLong, TapTin_DaXuLy);
                return true;
            }
            return false;
        }
        String[] DanhSach_DinhNghia = dinhnghia.split("\\|");
        TiengLongModel tl = new TiengLongModel(tulong, dinhnghia, DanhSach_DinhNghia);
        DanhSachTiengLong.put(tulong, tl);
        GhiDuLieu_TiengLong(DanhSachTiengLong, TapTin_DaXuLy);
        return true;
    }
    
    private LinkedHashMap<String, TiengLongModel> TimKiemTiengLongTheo_TuLong_DinhNghia(String tukhoa, int loaitimkiem){
        LinkedHashMap<String, TiengLongModel> TiengLong_TimKiem = new LinkedHashMap<>();
        Set<String> keySet = DanhSachTiengLong.keySet();
        
        switch(loaitimkiem) {
            case 1:
                for(String key : keySet){
                    TiengLongModel tienglong = DanhSachTiengLong.get(key);
                    if(tienglong.getTuLong().toLowerCase().contains(tukhoa.toLowerCase())) {
                        TiengLong_TimKiem.put(tienglong.getTuLong(), tienglong);
                    }
                }
                return TiengLong_TimKiem;
            case 2:
                for(String key : keySet){
                    TiengLongModel tienglong = DanhSachTiengLong.get(key);
                    if(tienglong.getDinhNghia().toLowerCase().contains(tukhoa.toLowerCase())) {
                        TiengLong_TimKiem.put(tienglong.getTuLong(), tienglong);
                    }
                }
                return TiengLong_TimKiem;
            default:
                return TiengLong_TimKiem;
        }
    }
    
    private void GhiDuLieu_TiengLong(LinkedHashMap<String, TiengLongModel> danhsach_tienglong, String tentaptin){
        Set<String> keySet = danhsach_tienglong.keySet();
        
        FileOutputStream fos = null;
        BufferedWriter bw = null;
        try {
            fos = new FileOutputStream(tentaptin);
            bw = new BufferedWriter(new OutputStreamWriter(fos, StandardCharsets.UTF_8));
            
            for(String key : keySet) {
                TiengLongModel tienglong = danhsach_tienglong.get(key);
                
                try {
                    bw.append(tienglong.getTuLong()+"`"+tienglong.getDinhNghia()+"\n");
                } catch (IOException ex) {
                    ex.getMessage();
                }
            }
        } catch (FileNotFoundException ex) {
            ex.getMessage();
        } finally{
            try {
                bw.flush();
                bw.close();
            } catch (IOException ex) {
                ex.getMessage();
            }
        }
    }
    
    private LinkedHashMap<String, TiengLongModel> DocDuLieu_TiengLong(String tentaptin){
        LinkedHashMap<String, TiengLongModel> TiengLong = new LinkedHashMap<>();
        
        if(!KiemTraTapTin(tentaptin)){
            return TiengLong;
        } 
            FileInputStream fis = null;
            BufferedReader br = null;
            try {
                fis = new FileInputStream(tentaptin);
                br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
                String s;
                while ((s = br.readLine()) != null) {
                    String[] TiengLong_DinhNghia = s.split("`");
                    if(TiengLong_DinhNghia.length > 1) {
                        String TuLong = TiengLong_DinhNghia[0];
                        String DinhNghia = TiengLong_DinhNghia[1];
                        String[] DanhSach_DinhNghia = DinhNghia.split("\\|");
                        TiengLongModel tl = new TiengLongModel(TuLong, DinhNghia, DanhSach_DinhNghia);
                        TiengLong.put(TuLong, tl);
                    }
                }
                System.out.println("Has read the file processed successfully!!!!!!");
            }catch (IOException ex) {
                ex.getMessage();
                ex.printStackTrace();
            }finally{
                try {
                    fis.close();
                    br.close();
                } catch (IOException ex) {
                    ex.getMessage();
                }
            }
        
        return TiengLong;
    }
    
    private void GhiDuLieu_LichSu_TimKiem(ArrayList<LichSuTimKiemModel> lichsu_timkiem, String tentaptin){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(tentaptin);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(lichsu_timkiem);
            System.out.println("Ghi lịch sử tìm kiếm thành công...");
        } catch (FileNotFoundException e) {
            System.out.println("Không tìm thấy tập tin: " + e);
        } catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try {
                oos.close();
                fos.close();
            } catch (IOException ex) {
                ex.getMessage();
            }
        }
    }
    
    private ArrayList<LichSuTimKiemModel> DocDuLieu_LichSu_TimKiem(String tentaptin){
        ArrayList<LichSuTimKiemModel> KetQuaTimKiem = new ArrayList<>();
        if(!KiemTraTapTin(tentaptin)) {
            return KetQuaTimKiem;
        }
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(tentaptin);
            ois = new ObjectInputStream(fis);
            KetQuaTimKiem = (ArrayList<LichSuTimKiemModel>) ois.readObject();
            System.out.println("Đọc lịch sử tìm kiếm thành công...");
        } catch (Exception e) {
            return KetQuaTimKiem;
        }
        finally{
            try {
                if(ois != null)
                    ois.close();
                if(fis != null)
                    fis.close();
            } catch (IOException ex) {
                ex.getMessage();
            }
        }
        return KetQuaTimKiem;
    }
    
    private void GhiDuLieu_MoiNgayMotTuLong(String tulongmoingay, String tentaptin){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(tentaptin);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(tulongmoingay);
        } catch (FileNotFoundException e) {
            System.out.println("Không tìm thấy tập tin: " + e);
        } catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try {
                oos.close();
                fos.close();
            } catch (IOException ex) {
                ex.getMessage();
            }
        }
    }
    
    private String DocDuLieu_MoiNgayMotTuLong(String tentaptin){
        String TuLongMoiNgay = "";
        if(!KiemTraTapTin(tentaptin)) {
            return TuLongMoiNgay;
        }
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(tentaptin);
            ois = new ObjectInputStream(fis);
            TuLongMoiNgay = (String) ois.readObject();
        } catch (Exception e) {
            return "";
        }
        finally{
            try {
                if(ois != null)
                    ois.close();
                if(fis != null)
                    fis.close();
            } catch (IOException ex) {
                ex.getMessage();
            }
        }
        return TuLongMoiNgay;
    }
    
    private LinkedHashMap KhoiTao_LamSach_DuLieu() {
        if(!KiemTraTapTin(TapTin_DaXuLy)) {
            LamSachDuLieu();
        }
        return DocDuLieu_TiengLong(TapTin_DaXuLy);
    }
    
    private void LamSachDuLieu() {
        LinkedHashMap<String, TiengLongModel> TiengLong = DocDuLieu_TiengLong(TapTin_DuLieuChuaXuLy);
        TaoTapTin(TapTin_DaXuLy, 2);
        GhiDuLieu_TiengLong(TiengLong, TapTin_DaXuLy);
    }
    
    private boolean KiemTra_DinhNghia_TonTai_DanhSachDinhNghia(String[] danhsach_dinhnghia, String dinhnghia) {
        for (int i = 0; i < danhsach_dinhnghia.length; i++) {
            if(danhsach_dinhnghia[i].trim().toLowerCase().equals(dinhnghia.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
    
    private String[][] ChuyenDoiDuLieu(LinkedHashMap<String, TiengLongModel> danhsachtienglong) {
        Set<String> keySet = danhsachtienglong.keySet();
        
        String[][] DSTL = null;
        if (danhsachtienglong.size() <= 0) {
            return new String[][]{};
        }
        
        DSTL = new String[danhsachtienglong.size()][2];
        int i = 0;
        for (String key : keySet) {
            TiengLongModel tl = danhsachtienglong.get(key);
            DSTL[i] = new String[] {tl.getTuLong(), tl.getDinhNghia()};
            i++;
        }
        
        return DSTL;
    }
    
    private String ChuyenDoiDinhNghia(String[] dinhnghia) {
        String s1 = dinhnghia[0];
        for (int i = 1; i < dinhnghia.length; i++) {
            s1 = s1 + " | " + dinhnghia[i];
        }
        return s1;
    }
    
    private String LayNgayThangNam(String format) {
        DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern(format);
        LocalDateTime localDateTime = LocalDateTime.now();
        return FOMATTER.format(localDateTime);
    }
    
    private void TaoTapTin(String file_name, int lua_chon) {
        File file = new File(file_name);
        switch (lua_chon) {
            case 1:
                if (!file.exists()) {
                    file.mkdir();
                }
            case 2:
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        System.out.println("Tap tin loi" + e);
                    }
                }
            default:
                return;
        }
    }
    
    private boolean KiemTraTapTin(String file_name) {
        File file = new File(file_name);
        if (!file.exists()) {
            return false;
        }
        return true;
    }
}
