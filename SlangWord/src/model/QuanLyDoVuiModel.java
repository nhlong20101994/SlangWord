
package model;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;


public class QuanLyDoVuiModel {
    private LinkedHashMap<String, TiengLongModel> DanhSachTiengLong;
    private int LoaiDoVui;
    
    public QuanLyDoVuiModel(LinkedHashMap<String, TiengLongModel> danhsachtienglong, int loaidovui) {
        DanhSachTiengLong = danhsachtienglong;
        LoaiDoVui = loaidovui;
    }
    
    public DoVuiModel LayCauDo(){
        DoVuiModel DoVui = null;
        ArrayList<String> DanhSachTuLong = new ArrayList<>(DanhSachTiengLong.keySet());
        Collections.shuffle(DanhSachTuLong);
        if(LoaiDoVui == 1){
            DanhSachTuLong = new ArrayList<>(DanhSachTuLong.subList(0, 4));
            TiengLongModel TiengLong = DanhSachTiengLong.get(DanhSachTuLong.get(0));
            String CauHoi = TiengLong.getTuLong();
            int ranNum = ThreadLocalRandom.current().nextInt(0,TiengLong.getDanhSachDinhNghia().length);
            String KetQua = TiengLong.getDanhSachDinhNghia()[ranNum];
            
            ArrayList<String> CauTraLoi = new ArrayList<>();
            for (String key : DanhSachTuLong) {
                TiengLongModel TiengLong_CTL = DanhSachTiengLong.get(key);
                int ranNum_CTL = ThreadLocalRandom.current().nextInt(0,TiengLong_CTL.getDanhSachDinhNghia().length);
                CauTraLoi.add(DanhSachTiengLong.get(key).getDanhSachDinhNghia()[ranNum_CTL]);
            }
            Collections.shuffle(CauTraLoi);
            DoVui = new DoVuiModel(CauHoi, CauTraLoi, KetQua);
        }
        if(LoaiDoVui == 2){
            DanhSachTuLong = new ArrayList<>(DanhSachTuLong.subList(0, 4));
            TiengLongModel TiengLong = DanhSachTiengLong.get(DanhSachTuLong.get(0));
            int ranNum = ThreadLocalRandom.current().nextInt(0,TiengLong.getDanhSachDinhNghia().length);
            String CauHoi = TiengLong.getDanhSachDinhNghia()[ranNum];
            String KetQua = TiengLong.getTuLong();
            
            ArrayList<String> CauTraLoi = new ArrayList<>();
            for (String key : DanhSachTuLong) {
                CauTraLoi.add(DanhSachTiengLong.get(key).getTuLong());
            }
            Collections.shuffle(CauTraLoi);
            DoVui = new DoVuiModel(CauHoi, CauTraLoi, KetQua);
        }
        return DoVui;
    }
}
