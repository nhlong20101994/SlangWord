
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import view.QuanLyTiengLongView;

public class QuanLyTiengLongController implements ActionListener, KeyListener {
    private QuanLyTiengLongView QLTL;
    
    public QuanLyTiengLongController(QuanLyTiengLongView qltl) {
        QLTL =  qltl;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String src = e.getActionCommand();
        switch(src) {
            case "Tìm kiếm":
                QLTL.TimKiemTheo_TuLong_DinhNghia();
                return;
            case "Thêm":
                QLTL.ThemTiengLong();
                return;
            case "Cập nhật":
                QLTL.CapNhatTiengLong();
                return;
            case "<html><center>Chạy lại <br>tập tin gốc":
                QLTL.ChayLaiTepTinGoc();
                return;
            case "<html><center>Lịch sử <br>tìm kiếm":
                QLTL.XemLichSuTimKiem();
                return;
            case "Xóa":
                QLTL.XoaTiengLong();
                return;
            case "<html><center>Đố vui <br>từ lóng":
                QLTL.DoVui_TuLong();
                return;
            case "<html><center>Đố vui <br>định nghĩa":
                QLTL.DoVui_DinhNghia();
                return;
            case "Xem":
                QLTL.CapNhat_Xem_An(src);
                return;
            case "Ẩn":
                QLTL.CapNhat_Xem_An(src);
                return;
            default:
                return;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(QLTL.getJTextField_TimKiem().equals("")){
            QLTL.CapNhat_Xem_An("Ẩn");
        }
    }
    
}
