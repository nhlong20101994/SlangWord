
package controller;

import java.awt.event.*;
import view.CapNhatTiengLongView;

public class CapNhatTiengLongController implements ActionListener, WindowListener {
    private CapNhatTiengLongView CNTL;
    
    public CapNhatTiengLongController(CapNhatTiengLongView cntl) {
        CNTL = cntl;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String src = e.getActionCommand();
        switch(src) {
            case "Cập nhật":
                CNTL.CapNhatTiengLong();
                return;
            default:
                return;
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
        
    }

    @Override
    public void windowClosing(WindowEvent e) {
        CNTL.DongCuaSo_CapNhatTiengLong();
    }

    @Override
    public void windowClosed(WindowEvent e) {
        
    }

    @Override
    public void windowIconified(WindowEvent e) {
        
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        
    }

    @Override
    public void windowActivated(WindowEvent e) {
        
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        
    }
}
