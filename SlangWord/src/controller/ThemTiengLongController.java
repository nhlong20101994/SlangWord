
package controller;

import java.awt.event.*;
import view.ThemTiengLongView;

public class ThemTiengLongController implements ActionListener, WindowListener {
    private ThemTiengLongView TTL;
    
    public ThemTiengLongController(ThemTiengLongView ttl) {
        TTL = ttl;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String src = e.getActionCommand();
        switch(src) {
            case "Thêm":
                TTL.ThemTiengLong();
                return;
            default:
                return;
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
        
    }

    @Override
    public void windowClosing(WindowEvent e) {
        TTL.DongCuaSo_ThemTiengLong();
    }

    @Override
    public void windowClosed(WindowEvent e) {
        
    }

    @Override
    public void windowIconified(WindowEvent e) {
        
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        
    }

    @Override
    public void windowActivated(WindowEvent e) {
        
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        
    }
}
