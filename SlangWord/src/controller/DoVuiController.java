
package controller;

import java.awt.event.*;
import view.DoVuiView;

public class DoVuiController  implements ActionListener, WindowListener {
    private DoVuiView DV;
    
    public DoVuiController(DoVuiView dv) {
        DV = dv;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String src = e.getActionCommand();
        if(src.equals("Random")) {
            DV.RaCauDo_TuDong();
            return;
        }
        if(src.substring(0, 6).equals("Đáp án")) {
            DV.KiemTraKetQua(src.substring(10));
            return;
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
        
    }

    @Override
    public void windowClosing(WindowEvent e) {
        DV.DongCuaSo_DoVui();
    }

    @Override
    public void windowClosed(WindowEvent e) {
        
    }

    @Override
    public void windowIconified(WindowEvent e) {
        
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        
    }

    @Override
    public void windowActivated(WindowEvent e) {
        
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        
    }
}
