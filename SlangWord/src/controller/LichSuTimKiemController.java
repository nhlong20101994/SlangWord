
package controller;

import java.awt.event.*;
import javax.swing.event.*;
import view.LichSuTimKiemView;

public class LichSuTimKiemController implements ActionListener, WindowListener, ListSelectionListener {
    private LichSuTimKiemView LSTK;
    
    public LichSuTimKiemController(LichSuTimKiemView lstk) {
        LSTK =  lstk;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String src = e.getActionCommand();
        switch(src) {
            case "Bỏ chọn":
                LSTK.BoChon_DongCuaSoDanhSach();
                return;
            default:
                return;
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        LSTK.XemKetQuaTimKiem();
    }
    
    @Override
    public void windowOpened(WindowEvent e) {
        
    }

    @Override
    public void windowClosing(WindowEvent e) {
        LSTK.DongCuaSo_ThemTiengLong();
    }

    @Override
    public void windowClosed(WindowEvent e) {
        
    }

    @Override
    public void windowIconified(WindowEvent e) {
        
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        
    }

    @Override
    public void windowActivated(WindowEvent e) {
        
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        
    }

}
